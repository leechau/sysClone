# nodejs
PATH=$PATH:$HOME/apps/node-v5.4.1-linux-x64/bin

# rvm, for octopress
PATH=$PATH:$HOME/.rvm/bin
source $HOME/.rvm/scripts/rvm

# haskell
PATH=$PATH:$HOME/.stack/programs/x86_64-linux/ghc-7.10.2/bin:$HOME/.local/bin:$HOME/.cabal/bin

# scala
PATH=$PATH:$HOME/apps/scala-2.11.7/bin

# go
GOROOT=/usr/local/go
PATH=$PATH:$GOROOT/bin

# VS Code
PATH=$PATH:$HOME/apps/VSCode-linux-x64
